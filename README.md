# ThinkPHP 5.1 连接达梦数据库源码

## 简介

本仓库提供了一个用于在ThinkPHP 5.1框架中连接达梦数据库的源码示例。通过本示例，您可以快速了解如何在ThinkPHP 5.1项目中配置和使用达梦数据库。

## 功能特点

- **ThinkPHP 5.1**：基于ThinkPHP 5.1框架，适用于PHP 7.1及以上版本。
- **达梦数据库**：支持连接达梦数据库，实现数据的增删改查操作。
- **简单易用**：提供详细的配置和使用说明，帮助开发者快速上手。

## 使用说明

### 1. 环境要求

- PHP 7.1 及以上版本
- ThinkPHP 5.1
- 达梦数据库（DM8）

### 2. 安装与配置

1. **克隆仓库**：
   ```bash
   git clone https://github.com/your-repo/tp5-dm-connector.git
   ```

2. **配置数据库连接**：
   在 `config/database.php` 文件中，配置达梦数据库的连接信息：
   ```php
   'connections' => [
       'dm' => [
           'type'     => 'dm',
           'hostname' => 'localhost',
           'database' => 'your_database',
           'username' => 'your_username',
           'password' => 'your_password',
           'hostport' => '5236', // 达梦数据库默认端口
           'params'   => [],
           'charset'  => 'utf8',
           'prefix'   => '',
       ],
   ],
   ```

3. **使用示例**：
   在控制器中使用数据库连接：
   ```php
   namespace app\index\controller;

   use think\Db;

   class Index
   {
       public function index()
       {
           $result = Db::connect('dm')->table('your_table')->select();
           return json($result);
       }
   }
   ```

### 3. 运行项目

1. 启动ThinkPHP内置服务器：
   ```bash
   php think run
   ```

2. 访问项目：
   ```
   http://localhost:8000
   ```

## 贡献

欢迎提交Issue和Pull Request，共同完善本项目。

## 许可证

本项目采用 [MIT 许可证](LICENSE)。